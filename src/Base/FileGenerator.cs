﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Base.Models;

namespace Base
{
    public class FileGenerator
    {
        /// <param name="fileSize">Размер файла в ГБ</param>
        public void Generate(int fileSize)
        {
            long maximumBytes = fileSize;
            maximumBytes = maximumBytes *1024 * 1024 * 1024;

            var fileName = "test.txt";
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            using var writer = File.CreateText("test.txt");
            var rowGenerator = new RowGenerator
            {
                Rnd = new Random()
            };

            while (writer.BaseStream.Position < maximumBytes)
            {
                rowGenerator.Generate();

                writer.Write(rowGenerator.Number);
                writer.Write('.');
                writer.WriteLine(new ReadOnlySpan<char>(rowGenerator.Name, 0, rowGenerator.Name.Length));
            }
        }
    }
}
