﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Models
{
    public struct RowGenerator
    {
        public Random Rnd;
        public int Number;
        public char[] Name;
        static int upperLetters = 'Z' - 'A';
        static int lowerLetters = 'z' - 'a';

        public void Generate()
        {

            var len = Rnd.Next(1, 50);
            Name = new char[len];
            Name[0] = (char)('A' + Rnd.Next(upperLetters));

            for (int i = 1; i < len; i++)
            {
                Name[i] = (char)('a' + Rnd.Next(lowerLetters));
            }
            Number = Rnd.Next(short.MaxValue);
        }
    }
}
