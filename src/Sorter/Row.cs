﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sorter
{
    public class Row
    {
        public string Title { get; set; }
        public int Number { get; set; }
    }
}
