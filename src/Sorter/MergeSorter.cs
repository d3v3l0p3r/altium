﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;

namespace Sorter
{
    public class MergeSorter
    {
        private List<string> _items;
        private string[] _sorted;

        public void Add(string item)
        {
            _items.Add(item);
            _sorted = MergeSort(_items.ToArray(), 0, _items.Count - 1);
        }

        public string[] Sort(string[] s)
        {
            MergeSort(s, 0, s.Length - 1);
            return s;
        }

        private string[] MergeSort(string[] s, int lowIndex, int highIndex)
        {
            if (lowIndex < highIndex)
            {
                var middleIndex = (lowIndex + highIndex) / 2;
                MergeSort(s, lowIndex, middleIndex);
                MergeSort(s, middleIndex + 1, highIndex);
                Merge(s, lowIndex, middleIndex, highIndex);
            }

            return s;
        }

        private void Merge(string[] array, int lowIndex, int middleIndex, int highIndex)
        {
            var left = lowIndex;
            var right = middleIndex + 1;
            var tempArray = new string[highIndex - lowIndex + 1];
            var index = 0;

            while ((left <= middleIndex) && (right <= highIndex))
            {
                var comp = Compare(array[left], array[right]);
                if (comp < 0)
                {
                    tempArray[index] = array[left];
                    left++;
                }
                else if (comp > 0)
                {
                    tempArray[index] = array[right];
                    right++;
                }
                else
                {
                    tempArray[index] = array[left];
                    left++;
                }

                index++;
            }

            for (int i = left; i <= middleIndex; i++)
            {
                tempArray[index] = array[i];
                index++;
            }

            for (int i = right; i <= highIndex; i++)
            {
                tempArray[index] = array[i];
                index++;
            }

            for (var i = 0; i < tempArray.Length; i++)
            {
                array[lowIndex + i] = tempArray[i];
            }
        }

        public static int Compare(string s1, string s2)
        {
            var leftValue = s1.Split('.');
            var rightValue = s2.Split('.');

            var comp = String.Compare(leftValue[1], rightValue[1], StringComparison.Ordinal);
            if (comp != 0)
                return comp;

            int leftNumber = int.Parse(leftValue[0]);
            int rightNumber = int.Parse(rightValue[0]);

            if (leftNumber < rightNumber)
            {
                return -1;
            }

            if (leftNumber > rightNumber)
            {
                return +1;
            }

            return 0;
        }

        public string GetMinimumIndex(string[] array)
        {
            MergeSort(array, 0, array.Length - 1);

            return array[0];
        }

    }
}
