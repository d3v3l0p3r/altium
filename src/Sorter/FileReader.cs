﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sorter
{
    public class FileReader
    {
        private const int RowsInFile = 1000000;
        private const string TempDirectoryName = "temp";
        private readonly MergeSorter _sorter = new MergeSorter();

        public long Read()
        {
            if (Directory.Exists(TempDirectoryName))
            {
                Directory.Delete(TempDirectoryName, true);
            }

            Directory.CreateDirectory(TempDirectoryName);

            SplitFiles(out int filesCount, out long totalLines);
            SortFiles(filesCount);
            MergeFiles(filesCount, 0);

            return totalLines;
        }

        private void SplitFiles(out int filesCount, out long totalLines)
        {
            var watch = Stopwatch.StartNew();
            watch.Start();
            totalLines = 0;
            filesCount = 0;
            using var reader = File.OpenText("test.txt");
            StreamWriter writer = null;

            while (!reader.EndOfStream)
            {
                if (totalLines % RowsInFile == 0)
                {
                    writer?.Dispose();
                    writer = File.CreateText($"temp/{filesCount}.text");
                    filesCount++;
                }

                var row = reader.ReadLine();
                writer.WriteLine(row);
                totalLines++;
            }

            writer?.Dispose();
            watch.Stop();
            Console.WriteLine("Split files: " + watch.ElapsedMilliseconds / 1000);
        }

        private void SortFiles(int filesCount)
        {
            var watch = Stopwatch.StartNew();
            watch.Start();

            var tasks = new Task[filesCount];
            for (var i = 0; i < filesCount; i++)
            {
                var i1 = i;
                tasks[i] = Task.Run(() =>
                {
                    string fileName = $"{TempDirectoryName}/{i1}.text";
                    using var reader = File.OpenText(fileName);
                    var fileSequence = new List<string>(RowsInFile);

                    while (!reader.EndOfStream)
                    {
                        fileSequence.Add(reader.ReadLine());
                    }

                    reader.Dispose();
                    File.Delete(fileName);

                    var sw = File.CreateText($"{TempDirectoryName}/{i1}.sorted.text");

                    var fileSequenceArray = fileSequence.ToArray();
                    fileSequenceArray = _sorter.Sort(fileSequenceArray);
                    foreach (var t in fileSequenceArray)
                    {
                        sw.WriteLine(t);
                    }

                    sw.Dispose();
                });
            }
            Task.WhenAll(tasks).Wait();
            watch.Stop();
            Console.WriteLine("Sort files: " + watch.ElapsedMilliseconds / 1000);
        }

        private void MergeFiles(int filesCount, int iteration)
        {
            for (int i = 0; i < filesCount;)
            {
                MergeFiles(i, i + 1, iteration);
                i += 2;
                if (i > filesCount)
                {
                    break;
                }
            }

            iteration++;
            MergeFiles(filesCount / 2, iteration);
        }

        private void MergeFiles(int index1, int index2, int iteration)
        {
            var fileName1 = iteration != 0 ? $"{TempDirectoryName}/{index1}.{iteration-1}.sorted.text" : $"{TempDirectoryName}/{index1}.sorted.text";
            var fileName2 = iteration != 0 ? $"{TempDirectoryName}/{index2}.{iteration-1}.sorted.text" : $"{TempDirectoryName}/{index2}.sorted.text";
            var sr1 = new StreamReader(new FileStream(fileName1, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.SequentialScan));
            var sr2 = new StreamReader(new FileStream(fileName2, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.SequentialScan));
            var sw = new StreamWriter(new FileStream($"{TempDirectoryName}/{index1}.{iteration}.sorted.text", FileMode.CreateNew, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan));

            var value1 = sr1.ReadLine();
            var value2 = sr2.ReadLine();

            while (!sr1.EndOfStream && !sr2.EndOfStream)
            {
                var comp = MergeSorter.Compare(value1, value2);
                if (comp <= 0)
                {
                    sw.WriteLine(value1);
                    value1 = sr1.ReadLine();
                }

                if (comp > 0)
                {
                    sw.WriteLine(value2);
                    value2 = sr2.ReadLine();
                }
            }

            if (!sr1.EndOfStream && sr2.EndOfStream)
            {
                while (!sr1.EndOfStream)
                {
                    sw.WriteLine(sr1.ReadLine());
                }
            }

            if (sr1.EndOfStream && !sr2.EndOfStream)
            {
                while (!sr2.EndOfStream)
                {
                    sw.WriteLine(sr2.ReadLine());
                }
            }
        }


    }
}
