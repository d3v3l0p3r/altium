﻿using System;
using System.Diagnostics;
using System.IO;

namespace Sorter
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = Stopwatch.StartNew();
            var fileReader = new FileReader();

            Console.WriteLine("Hello User!");
            Console.WriteLine("Скопируй файл test.txt в директорию программы Sorter и нажми Enter.");

            Console.ReadLine();
            Console.WriteLine("Сортировка начата.");
            watch.Start();
            var lines = fileReader.Read();
            watch.Stop();

            Console.WriteLine("Total lines: " + lines);
            Console.WriteLine("Total elapsed seconds: " + watch.ElapsedMilliseconds / 1000);
            Console.ReadLine();
        }
    }
}
