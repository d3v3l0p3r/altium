﻿using System;
using System.Diagnostics;

namespace Generator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello user!");

            var watch = Stopwatch.StartNew();
            int fileSize;

            while (true)
            {
                Console.WriteLine("Введите размер конечного файла в гигабайтах");
                var inputSize = Console.ReadLine();
                if (int.TryParse(inputSize, out fileSize))
                {
                    break;
                }
            }

            watch.Start();
            var fileGenerator = new FileGenerator();
            fileGenerator.Generate(fileSize);
            watch.Stop();
            Console.WriteLine("Total elapsed seconds: " + watch.ElapsedMilliseconds / 1000);
            Console.ReadLine();
        }
    }
}
