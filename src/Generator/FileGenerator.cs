﻿using System;
using System.IO;

namespace Generator
{
    public class FileGenerator
    {
        /// <param name="fileSize">Размер файла в ГБ</param>
        public void Generate(int fileSize)
        {
            long maximumBytes = fileSize * 1024L * 1024 * 1024;

            var fileName = "test.txt";
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            using var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None, 8192);
            using var writer = new StreamWriter(fs);
            var rowGenerator = new RowGenerator
            {
                Rnd = new Random()
            };

            while (writer.BaseStream.Position < maximumBytes)
            {
                rowGenerator.Generate();

                writer.Write(rowGenerator.Number);
                writer.Write(new ReadOnlySpan<char>(new []{'.', ' '}));
                writer.WriteLine(new ReadOnlySpan<char>(rowGenerator.Name, 0, rowGenerator.Name.Length));
            }
        }
    }
}
